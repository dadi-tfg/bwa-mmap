#!/usr/bin/env bash

FASTAS=./fastas
LOGS=./logs
printf -v LOGFILE '%(%Y-%m-%d)T.log' -1
LOG="$LOGS/$LOGFILE"
BWA="./src/bwa"

clean_fasta () {
  rm *.fasta.*
  echo "Removed everything except FASTA files"
}

log () {
  printf "%(%H:%M:%S)T %s\n" -1 "$*" | tee -a ${LOG}
}

check_folders () {
  if [[ ! -d "$LOGS" ]]; then
    log "Folder $LOGS does not exist, exiting"
    exit 100
  fi
  touch "$LOG"
  log "Using log file ${LOG}"

  if [[ ! -d "$FASTAS" ]]; then
    log "Folder $FASTAS does not exist, exiting"
    log "SCRIPT END"
    exit 101
  fi

  if [ -z "$(ls -A $FASTAS)" ]; then
    log "No fasta file found in $FASTAS, exiting"
    log "SCRIPT END"
    exit 102
  fi

}

log "SCRIPT START"
check_folders
i=1
log "INDEX START"
for FASTA in $(ls $FASTAS/*.fasta); do
  log "Index $i: $FASTA start"
  $BWA index $FASTA 2>&1 | tee -a ${LOG}
  log "Index $i: $FASTA end"
  ((i+=1))
done
log "INDEX END"

# log "ALLIGN START"
# log "ALLIGN END"

log "SCRIPT END"
